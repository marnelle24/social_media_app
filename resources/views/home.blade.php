@extends('layouts.app')

@extends('layouts.nav')

@section('content')
<div class="container">
    <profile :user="{{ Auth::user() }}" ></profile>
    <div class="row justify-content-center">
        <div class="col-md-4">
            <center>
                <img src="{{ asset('images/pic.png') }}" width="150">    
            </center>
            <br />
            <div class="card">
                <div class="card-header text-center">
                    <h4>{{ Auth::user()->name }}</h4>
                </div>
                <div class="card-body">
                    <table class="table table-sm table-stripe">
                        <tr>
                            <td class="text-right">Phone</td>
                            <td class="text-left">: {{ Auth::user()->contact_number }}</td>
                        </tr>
                        <tr>
                            <td class="text-right">Email</td>
                            <td class="text-left">: {{ Auth::user()->email }}</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-center pt-4">
                                
                                <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#editProfile">Edit Profile</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-8">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>
                        Post successfully shared...
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </p>
                    
                </div>
            @endif

            <form method="POST" action="{{ route('post.create') }}">
                @csrf
                <div class="form-group">
                    <textarea class="form-control" name="content" placeholder="Share your knowledge my friend...." rows="4" required></textarea>
                </div>
                <div class="form-group clearfix">
                    <button type="submit" class="btn btn-primary float-right">POST</button>    
                </div>
            </form>

            <div class="card">
                <div class="card-header"><strong>NEWSFEEDS</strong></div>

                <div class="card-body">
                    @foreach($posts as $post)
                        <section class="row">
                            <div class="col-md-12 border border-light border-left-0 border-right-0 border-top-0 pb-3 pt-3">
                                <article>
                                    <p>{{ $post->content }}</p>
                                    <div class="text-muted mb-4">
                                        <sub><i>Posted by {{ $post->user->username }} last {{ $post->created_at }}</i></sub>
                                    </div>
                                    <div class="interaction">
                                        <engagement 
                                                 :post="{{ !empty(Auth::user()->likes()->where('post_id', $post->id)->first()) ? Auth::user()->likes()->where('post_id', $post->id)->first() : $post }}"
                                                 :user="{{ Auth::user() }}"
                                                 :comments="{{ !empty(Auth::user()->comments()->where('post_id', $post->id)->first()) ? Auth::user()->comments()->where('post_id', $post->id)->orderBy('created_at', 'DESC')->get() : $post}}">
                                        </engagement>
                                    </div>    
                                    
                                    
                                </article>
                            </div>

                        </section>
                    @endforeach                    
                </div>

            </div>


            <!-- <create-post></create-post> -->
            <!-- <news-feeds></news-feeds> -->
        </div>
    </div>
</div>

@endsection
