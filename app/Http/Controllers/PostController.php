<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function create(Request $request) 
    {
        $post = new Post;
        
        $validatePost   = $this->validate($request, ['content' => 'required']);
        $post->content  = $validatePost['content'];

        $request->user()->posts()->save($post);
        
        return redirect()->route('home')->with('success', 'Posted successfully..');
    }

}
