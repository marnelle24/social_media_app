<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $posts = Post::orderBy('created_at', 'desc')->get(); 
        return view('home', compact('posts', $posts));
    }


    // update user profile information
    public function editProfile(Request $request) {

        return $request->all(); 
        
        // $user = new User;

        // $validateData = $this->validate($request, [
        //     'name'          => 'required',
        //     'contact_number'=> 'required',
        //     'email'         => 'required|email'
        // ]);

        // $user->find($request['user_id'])->update($validateData);
        // return $user;
       
    }

}
