<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Like;

class LikeController extends Controller
{

    public function create(Request $request) 
    {

    	if(!empty($request['post_data']['post_id']))
    	{
    		$like = Like::where('post_id',$request['post_data']['post_id'])->first();
	    	if($like) {

	    		if($like->like == 0)
	    			$like->update(['like'=>1]);
	    		else
	    			$like->update(['like'=>0]);

	    	}
	    	return $like->like;
    	}
    	else {

	    	$newLike = new Like;

			$newLike->post_id = $request['post_data']['id'];
			$newLike->user_id = $request['currentUser'];
			$newLike->like    = 1;

			$newLike->save();

    		return $newLike->like;
    	}


		

    	


    }
    		
}
