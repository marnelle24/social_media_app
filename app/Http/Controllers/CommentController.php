<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class CommentController extends Controller
{
    public function create(Request $request) 
    {
    	$comment = new Comment;

    	$comment->post_id 		= $request['post_id'];
    	$comment->user_id 		= $request['currentUser'];
    	$comment->comment_body 	= $request['comment'];

    	$comment->save();
    	return $comment;
    }
}
