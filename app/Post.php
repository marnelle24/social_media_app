<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 
        'content'
    ];

    // get all the posts belongs to the user
    public function user() {
    	return $this->belongsTo('App\User');
    }

    // get all the likes of the posts
    public function likes() {
    	return $this->hasMany('App\Like');
    }

    public function comments() {
        return $this->hasMany('App\Comment');
    }

}
 