<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 
        'post_id',
        'like'
    ];


	// get the user info of this like record
	public function user() {
		return $this->belongsTo('App\User');
	}

	// get the post info of this like record
	public function post() {
		return $this->belongsTo('App\Post');
	}

}
