<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Like post
Route::post('/likePost', 'LikeController@create')->name('likePost');
Route::post('/saveComment', 'CommentController@create')->name('saveComment');
Route::post('/editProfile', 'HomeController@editProfile')->name('editProfile');

// });

